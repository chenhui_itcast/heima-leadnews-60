package com.heima.utils.thread;

import com.heima.model.wemedia.pojos.WmUser;

/**
 * @author 陈辉
 * @data 2023 15:10
 */

public class WmThreadLocalUtil {
    private static ThreadLocal<WmUser> threadLocal = new ThreadLocal<>();


    //存数据
    public static void setUser(WmUser wmUser){
        threadLocal.set(wmUser);
    }

    //取数据
    public static WmUser getUser(){
        return threadLocal.get();
    };

    //清空数据
    public static void clear(){
        threadLocal.remove();
    }
}

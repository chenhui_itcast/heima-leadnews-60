package com.heima.utils.thread;

import com.heima.model.wemedia.pojos.WmUser;

/**
 * @author 陈辉
 * @data 2023 15:10
 */

public class ApUserThreadLocalUtil {
    private static ThreadLocal<String> threadLocal = new ThreadLocal<>();


    //存数据
    public static void setUser(String userId){
        threadLocal.set(userId);
    }

    //取数据
    public static String getUser(){
        return threadLocal.get();
    };

    //清空数据
    public static void clear(){
        threadLocal.remove();
    }
}

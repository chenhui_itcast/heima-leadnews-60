package com.heima.search.controller.v1;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.search.service.SearchHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author 陈辉
 * @data 2023 15:24
 */
@RestController
@RequestMapping("/api/v1/history")
public class SearchHistoryController {
    @Autowired
    private SearchHistoryService searchHistoryService;

    /**
     * app用户搜索历史记录列表
     *
     * @return
     */
    @PostMapping("/load")
    public ResponseResult load(){
        return searchHistoryService.load();
    }

    /**
     * app搜索历史 - 删除搜索记录
     */
    @PostMapping("/del")
    public ResponseResult del(@RequestBody Map map){
        return searchHistoryService.del(map);
    }
}

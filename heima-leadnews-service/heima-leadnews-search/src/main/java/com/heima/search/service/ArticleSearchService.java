package com.heima.search.service;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.search.dtos.UserSearchDto;

/**
 * @author 陈辉
 * @data 2023 11:03
 */
public interface ArticleSearchService {


    /**
     * app文章搜索
     *
     * @param searchDto
     * @return
     */
    ResponseResult search(UserSearchDto searchDto);
}

package com.heima.search.controller.v1;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.search.dtos.UserSearchDto;
import com.heima.search.service.ArticleSearchService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 陈辉
 * @data 2023 10:35
 */
@RestController
@RequestMapping("/api/v1/article/search")
@Slf4j
public class ArticleSearchController {

    @Autowired
    private ArticleSearchService articleSearchService;

    /**
     * app文章搜素
     *
     * @param searchDto
     * @return
     */
    @PostMapping("/search")
    public ResponseResult searchArticle(@RequestBody UserSearchDto searchDto) {
        log.info("app文章开始搜素，搜素关键字是：{}", searchDto.getSearchWords());
        return articleSearchService.search(searchDto);
    }
}

package com.heima.search.service;

import com.heima.model.common.dtos.ResponseResult;

import java.util.Map;

/**
 * @author 陈辉
 * @data 2023 15:32
 */
public interface SearchHistoryService {
    /**
     * 加载app搜索历史记录
     *
     * @return
     */
    ResponseResult load();

    /**
     * 删除历史记录
     *
     * @param map
     * @return
     */
    ResponseResult del(Map map);

    /**
     * 保存搜索记录
     */
    void save(String searchWord);
}

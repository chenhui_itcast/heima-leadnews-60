package com.heima.search.config;

import com.heima.search.interceptor.ApUserTokenInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author 陈辉
 * @data 2023 16:12
 */
@Configuration
public class WebmvcConfig implements WebMvcConfigurer {
    @Autowired
    private ApUserTokenInterceptor apUserTokenInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(apUserTokenInterceptor);
    }
}

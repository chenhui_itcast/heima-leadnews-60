package com.heima.wemedia.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.wemedia.pojos.WmSensitive;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author 陈辉
 * @data 2023 9:36
 */
@Mapper
public interface WmSensitiveMapper extends BaseMapper<WmSensitive> {
}

package com.heima.wemedia.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.dtos.WmNewsDto;
import com.heima.model.wemedia.dtos.WmNewsPageReqDto;
import com.heima.model.wemedia.pojos.WmNews;

/**
 * @author 陈辉
 * @data 2023 10:27
 */
public interface WmNewsService extends IService<WmNews> {
    /**
     * 内容列表 -- 多条件分页查询
     *
     * @param newsPageReqDto
     * @return
     */
    ResponseResult findByPage(WmNewsPageReqDto newsPageReqDto);

    /**
     * 发布文章
     *
     * @param wmNewsDto
     * @return
     */
    ResponseResult submit(WmNewsDto wmNewsDto);

    /**
     * 文章上下架
     *
     * @param wmNewsDto
     * @return
     */
    ResponseResult downOrUp(WmNewsDto wmNewsDto);
}

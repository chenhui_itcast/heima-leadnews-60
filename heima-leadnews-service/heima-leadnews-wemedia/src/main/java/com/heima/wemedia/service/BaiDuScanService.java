package com.heima.wemedia.service;

/**
 * @author 陈辉
 * @data 2023 10:16
 */
public interface BaiDuScanService {

    /**
     * 百度云文本审核
     * @param text
     * @return 1：合规; 2: 不合规; 3: 疑似
     */
    int scanText(String text);

    /**
     * 百度云图片审核
     * @param path
     * @return 1：合规; 2: 不合规; 3: 疑似
     */
    int scanImg(String path);
}

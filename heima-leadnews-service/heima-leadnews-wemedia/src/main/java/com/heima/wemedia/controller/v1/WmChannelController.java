package com.heima.wemedia.controller.v1;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.wemedia.service.WmChannelService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 陈辉
 * @data 2023 9:09
 */
@RestController
@RequestMapping("/api/v1/channel")
@Slf4j
public class WmChannelController {
    @Autowired
    private WmChannelService wmChannelService;

    /**
     * 获取频道列表
     *
     * @return
     */
    @GetMapping("channels")
    public ResponseResult channels() {
        log.info("获取频道列表接口被请求了....");
        return wmChannelService.findAll();
    }
}

package com.heima.wemedia.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.wemedia.pojos.WmMaterial;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author 陈辉
 * @data 2023 11:24
 */
@Mapper
public interface WmMaterialMapper extends BaseMapper<WmMaterial> {
}

package com.heima.wemedia.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.dtos.WmMaterialDto;
import com.heima.model.wemedia.pojos.WmMaterial;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author 陈辉
 * @data 2023 11:25
 */
public interface WmMaterialService extends IService<WmMaterial> {
    /**
     * 素材管理 - 上传素材
     * @param multipartFile
     * @return
     */
    ResponseResult upload(MultipartFile multipartFile);

    /**
     * 素材管理- 素材列表
     * @param wmMaterialDto
     * @return
     */
    ResponseResult findByPage(WmMaterialDto wmMaterialDto);
}

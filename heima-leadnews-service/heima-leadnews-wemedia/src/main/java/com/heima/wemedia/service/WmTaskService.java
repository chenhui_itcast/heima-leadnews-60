package com.heima.wemedia.service;

import com.heima.model.wemedia.pojos.WmNews;

/**
 * @author 陈辉
 * @data 2023 16:14
 */
public interface WmTaskService {
    /**
     * 添加任务
     *
     * @param wmNews
     */
    void addTask(WmNews wmNews);

    /**
     * 定时拉取任务：文章审核
     */
    void popTask();
}

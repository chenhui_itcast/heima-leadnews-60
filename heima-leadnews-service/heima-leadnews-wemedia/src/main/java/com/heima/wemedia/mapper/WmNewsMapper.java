package com.heima.wemedia.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.wemedia.pojos.WmNews;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author 陈辉
 * @data 2023 10:27
 */
@Mapper
public interface WmNewsMapper extends BaseMapper<WmNews> {
}

package com.heima.wemedia.controller.v1;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.dtos.WmNewsDto;
import com.heima.model.wemedia.dtos.WmNewsPageReqDto;
import com.heima.wemedia.service.WmNewsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 陈辉
 * @data 2023 10:11
 */
@RestController
@RequestMapping("/api/v1/news")
@Slf4j
public class WmNewsController {

    @Autowired
    private WmNewsService wmNewsService;

    /**
     * 文章管理 - 内容列表
     *
     * @param newsPageReqDto
     * @return
     */
    @PostMapping("/list")
    public ResponseResult list(@RequestBody WmNewsPageReqDto newsPageReqDto) {
        log.info("文章管理 - 内容列表接口被请求了，请求参数：{}",newsPageReqDto);
        return wmNewsService.findByPage(newsPageReqDto);
    }

    /**
     * 文章管理 - 发布文章
     *
     * @return
     */
    @PostMapping("/submit")
    public ResponseResult submit(@RequestBody WmNewsDto wmNewsDto){
        log.info("文章管理 - 发布文章接口被请求了，请求参数：{}",wmNewsDto);
        return wmNewsService.submit(wmNewsDto);
    }

    /**
     * 文章管理 -  文章上下架
     *
     * @param wmNewsDto
     * @return
     */
    @PostMapping("/down_or_up")
    public ResponseResult downOrUp(@RequestBody WmNewsDto wmNewsDto){
        log.info("文章上下架功能处理：{}",wmNewsDto);
        return wmNewsService.downOrUp(wmNewsDto);
    }
}

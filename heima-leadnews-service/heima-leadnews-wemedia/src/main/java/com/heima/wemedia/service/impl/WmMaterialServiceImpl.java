package com.heima.wemedia.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.common.exception.CustomException;
import com.heima.file.service.FileStorageService;
import com.heima.model.common.dtos.PageResponseResult;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.wemedia.dtos.WmMaterialDto;
import com.heima.model.wemedia.pojos.WmMaterial;
import com.heima.utils.thread.WmThreadLocalUtil;
import com.heima.wemedia.mapper.WmMaterialMapper;
import com.heima.wemedia.service.WmMaterialService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Date;
import java.util.UUID;

/**
 * @author 陈辉
 * @data 2023 11:25
 */
@Service
@Transactional
@Slf4j
public class WmMaterialServiceImpl extends ServiceImpl<WmMaterialMapper, WmMaterial> implements WmMaterialService {
    @Autowired
    private FileStorageService fileStorageService;

    /**
     * 素材管理 - 上传素材
     *
     * @param multipartFile
     * @return
     */
    @Override
    public ResponseResult upload(MultipartFile multipartFile) {
        //1. 参数校验
        if (multipartFile == null)
            throw new CustomException(AppHttpCodeEnum.PARAM_INVALID);

        //2. 数据处理  -- 文件名,后缀名
        String targetFileName = UUID.randomUUID().toString();
        String sourceFileName = multipartFile.getOriginalFilename(); //a.b.png
        String suffix = sourceFileName.substring(sourceFileName.lastIndexOf("."));  // .png

        //3. 将文件上传到minIO
        String imgUrl = null;
        try {
            imgUrl = fileStorageService.uploadImgFile("", targetFileName + suffix, multipartFile.getInputStream());
            System.out.println("imgUrl = " + imgUrl);
        } catch (IOException e) {
            e.printStackTrace();
        }

        //4. 将minIO返回的文件地址保存到素材表：wm_material
        WmMaterial material = new WmMaterial();
        //怎么拿当前用户id?   -- ThreaLocal获取
        material.setUserId(WmThreadLocalUtil.getUser().getId());

        material.setUrl(imgUrl);
        material.setType((short) 0);
        material.setIsCollection((short) 0);
        material.setCreatedTime(new Date());

        save(material);

        //5. 返回结果
        return ResponseResult.okResult(material);
    }

    /**
     * 素材管理- 素材列表
     *
     * @param wmMaterialDto
     * @return
     */
    @Override
    public ResponseResult findByPage(WmMaterialDto wmMaterialDto) {
        //1. 参数校验
        if (wmMaterialDto == null)
            throw new CustomException(AppHttpCodeEnum.PARAM_INVALID);
        wmMaterialDto.checkParam();

        //2. 构建分页page对象
        Page ipage = new Page(wmMaterialDto.getPage(), wmMaterialDto.getSize());

        //3. 构建query查询条件
        LambdaQueryWrapper<WmMaterial> wrapper = Wrappers.<WmMaterial>lambdaQuery().eq(wmMaterialDto.getIsCollection() != null,
                WmMaterial::getIsCollection, wmMaterialDto.getIsCollection());
        //3.1 按创建时间降序排列
        wrapper.orderByDesc(WmMaterial::getCreatedTime);

        //4. 调用page方法进行分页查询
        page(ipage,wrapper);


        //5. 封装结果返回
        PageResponseResult pageResponseResult = new PageResponseResult((int)ipage.getCurrent()
                ,(int)ipage.getSize(),(int)ipage.getTotal());
        pageResponseResult.setCode(AppHttpCodeEnum.SUCCESS.getCode());
        pageResponseResult.setData(ipage.getRecords());

        return pageResponseResult;
    }
}

package com.heima.wemedia.controller.v1;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.wemedia.dtos.WmMaterialDto;
import com.heima.utils.thread.WmThreadLocalUtil;
import com.heima.wemedia.service.WmMaterialService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author 陈辉
 * @data 2023 11:08
 */
@RestController
@RequestMapping("/api/v1/material")
@Slf4j
public class WmMaterialController {
    @Autowired
    private WmMaterialService wmMaterialService;

    /**
     * 上传素材图片
     *
     * @param multipartFile
     * @return
     */
    @PostMapping("/upload_picture")
    public ResponseResult uploadPicture(MultipartFile multipartFile) {
        log.info("接收到前端传递的图片上传数据：{}", multipartFile);
        return wmMaterialService.upload(multipartFile);
    }

    /**
     * 素材列表
     *
     * @param wmMaterialDto
     * @return
     */
    @PostMapping("/list")
    public ResponseResult list(@RequestBody WmMaterialDto wmMaterialDto){
        log.info("素材列表 -- 接收到前端入参数据：{}", wmMaterialDto);

        return wmMaterialService.findByPage(wmMaterialDto);
    }
}

package com.heima.wemedia.service;

/**
 * @author 陈辉
 * @data 2023 11:29
 */
public interface WmAutoScanService {

    /**
     * 自动审核文章
     * @param newsId 文章id
     */
    void autoScan(Integer newsId);
}

package com.heima.wemedia.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.wemedia.pojos.WmChannel;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author 陈辉
 * @data 2023 9:13
 */
@Mapper
public interface WmChannelMapper extends BaseMapper<WmChannel> {
}

package com.heima.wemedia;

import com.heima.wemedia.service.WmAutoScanService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author 陈辉
 * @data 2023 11:57
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class WmAutoScanServiceTest {

    @Autowired
    private WmAutoScanService wmAutoScanService;

    @Test
    public void testScan(){
        wmAutoScanService.autoScan(6237);
    }
}

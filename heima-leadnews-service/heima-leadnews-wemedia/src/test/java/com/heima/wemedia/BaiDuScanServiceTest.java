package com.heima.wemedia;

import com.heima.wemedia.service.BaiDuScanService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author 陈辉
 * @data 2023 10:40
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class BaiDuScanServiceTest {
    @Autowired
    private BaiDuScanService baiDuScanService;

    @Test
    public void testText(){
        int res = baiDuScanService.scanText("这个SB辅助，居然抢我人头....");

        System.out.println("res = " + res);
    }

    @Test
    public void testImg(){
        int res = baiDuScanService.scanImg("https://p8.itc.cn/images01/20210113/23b9851d08e94649878fa3ba578463a0.jpeg");

        System.out.println("res = " + res);
    }

}

package com.heima.schedule.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.schedule.pojos.Taskinfo;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author 陈辉
 * @data 2023 16:36
 */
@Mapper
public interface TaskinfoMapper  extends BaseMapper<Taskinfo> {
}

package com.heima.schedule.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.schedule.dtos.TaskDto;
import com.heima.model.schedule.pojos.Taskinfo;

/**
 * @author 陈辉
 * @data 2023 16:37
 */
public interface TaskinfoService extends IService<Taskinfo> {
    /**
     * 添加任务
     *
     * @param taskDto
     * @return
     */
    ResponseResult addTask(TaskDto taskDto);

    /**
     * 拉取任务
     *
     * @param taskType
     * @param priority
     * @return
     */
    ResponseResult pullTask(Integer taskType, Integer priority);
}

package com.heima.schedule.feign;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.schedule.dtos.TaskDto;
import com.heima.schedule.IScheduleClient;
import com.heima.schedule.service.TaskinfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 陈辉
 * @data 2023 16:53
 */
@RestController
public class ScheduleClient implements IScheduleClient {

    @Autowired
    private TaskinfoService taskinfoService;
    /**
     * 添加任务
     *
     * @param taskDto
     * @return
     */
    @Override
    @PostMapping("/api/v1/schedule/addTask")
    public ResponseResult addTask(TaskDto taskDto) {
        return taskinfoService.addTask(taskDto);
    }

    /**
     * 拉取任务
     *
     * @param taskType
     * @param priority
     * @return
     */
    @Override
    @GetMapping("/api/v1/schedule/pullTask")
    public ResponseResult pullTask(Integer taskType, Integer priority) {
        return taskinfoService.pullTask(taskType,priority);
    }
}

package com.heima.schedule.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.schedule.pojos.TaskinfoLogs;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author 陈辉
 * @data 2023 16:36
 */
@Mapper
public interface TaskinfoLogsMapper extends BaseMapper<TaskinfoLogs> {
}

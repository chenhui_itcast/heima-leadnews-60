package com.heima.schedule;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.schedule.dtos.TaskDto;
import com.heima.schedule.service.TaskinfoService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * @author 陈辉
 * @data 2023 10:12
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class TaskInfoServiceTest {

    @Autowired
    private TaskinfoService taskinfoService;

    @Test
    public void testAddTask() throws ParseException {
        for (int i = 0; i < 3; i++) {
            TaskDto taskDto = new TaskDto();
            taskDto.setTaskType(1001);
            taskDto.setPriority(1);
            taskDto.setParameters(String.valueOf(6248).getBytes(StandardCharsets.UTF_8));
            long executeTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2023-09-13 15:35:00").getTime();
            taskDto.setExecuteTime(executeTime);
            taskinfoService.addTask(taskDto);
        }

    }

    @Test
    public void testPullTask(){
        ResponseResult result = taskinfoService.pullTask(1001, 1);
        System.out.println("result = " + result);
    }
}

package com.heima.article.controller;

import com.heima.article.service.ApArticleService;
import com.heima.common.constants.ArticleConstants;
import com.heima.file.service.FileStorageService;
import com.heima.model.article.dtos.ArticleHomeDto;
import com.heima.model.common.dtos.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 陈辉
 * @data 2023 10:36
 */
@RestController
@RequestMapping("/api/v1/article")
public class ApArticleController {

    @Autowired
    private ApArticleService apArticleService;

    /**
     * app 文章首页加载
     * @param homeDto
     * @return
     */
    @PostMapping("/load")
    public ResponseResult load(@RequestBody ArticleHomeDto homeDto){
        return apArticleService.loadIndex(homeDto, ArticleConstants.LOADTYPE_LOAD_MORE);
    }

    /**
     * app 文章首页加载更多
     * @param homeDto
     * @return
     */
    @PostMapping("/loadmore")
    public ResponseResult loadmore(@RequestBody ArticleHomeDto homeDto){
        return apArticleService.load(homeDto, ArticleConstants.LOADTYPE_LOAD_MORE);
    }

    /**
     * app 文章首页加载更新
     * @param homeDto
     * @return
     */
    @PostMapping("/loadnew")
    public ResponseResult loadnew(@RequestBody ArticleHomeDto homeDto){
        return apArticleService.load(homeDto, ArticleConstants.LOADTYPE_LOAD_NEW);
    }
}

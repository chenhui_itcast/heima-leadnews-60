package com.heima.article.feign;

import com.heima.article.IArticleClient;
import com.heima.article.service.ApArticleService;
import com.heima.model.article.dtos.ArticleDto;
import com.heima.model.common.dtos.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 陈辉
 * @data 2023 14:52
 */
@RestController
public class ArticleClient implements IArticleClient {
    @Autowired
    private ApArticleService apArticleService;
    /**
     * article文章同步接口
     *
     * @param articleDto
     * @return
     */
    @Override
    @PostMapping("/api/v1/article/async")
    public ResponseResult saveOrUpdateArticle(ArticleDto articleDto) {
       /* while (!"abc".equals("cba")){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }*/
        return apArticleService.async(articleDto);
    }
}

package com.heima.article.job;

import com.heima.article.service.HotArticleService;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author jack
 * @data 2023 14:24
 */
@Component
@Slf4j
public class ComputeHotArticleJob {

    @Autowired
    private HotArticleService hotArticleService;

    @XxlJob("computeHotArticleHandler")
    public void computeHotArticle(){
        log.info("热点文章定时计算 begin.....");

        hotArticleService.computeHotArticle();

        log.info("热点文章定时计算 end.....");
    }
}

package com.heima.article.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.article.mapper.ApArticleConfigMapper;
import com.heima.article.mapper.ApArticleContentMapper;
import com.heima.article.mapper.ApArticleMapper;
import com.heima.article.service.ApArticleService;
import com.heima.article.service.FreemarkerService;
import com.heima.common.constants.ArticleConstants;
import com.heima.common.redis.CacheService;
import com.heima.model.article.dtos.ArticleDto;
import com.heima.model.article.dtos.ArticleHomeDto;
import com.heima.model.article.pojos.ApArticle;
import com.heima.model.article.pojos.ApArticleConfig;
import com.heima.model.article.pojos.ApArticleContent;
import com.heima.model.article.vos.ArticleVo;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * @author 陈辉
 * @data 2023 11:22
 */
@Service
@Transactional
@Slf4j
public class ApArticleServiceImpl extends ServiceImpl<ApArticleMapper, ApArticle> implements ApArticleService {

    @Autowired
    private ApArticleMapper apArticleMapper;
    @Autowired
    private ApArticleConfigMapper apArticleConfigMapper;
    @Autowired
    private ApArticleContentMapper apArticleContentMapper;
    @Autowired
    private FreemarkerService freemarkerService;
    @Autowired
    private CacheService cacheService;

    /**
     * 加载首页              type  1
     *
     * @param homeDto
     * @return 加载更新       type  2
     * @return 加载更多       type  1
     */
    @Override
    public ResponseResult load(ArticleHomeDto homeDto, int type) {
        //1. 参数校验
        if (homeDto == null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //2. 数据处理
        if (homeDto.getMaxBehotTime() == null){
            homeDto.setMaxBehotTime(new Date(0L));
        }
        if (homeDto.getMinBehotTime() == null){
            homeDto.setMinBehotTime(new Date());
        }
        if (homeDto.getSize() == null || homeDto.getSize() <= 0){
            homeDto.setSize(10);
        }else{
            homeDto.setSize(Math.min(homeDto.getSize(), 50));   //防止分页条数过大，限制每页最大50条
        }

        //3. 调用mapper层处理数据
        List<ApArticle> list = apArticleMapper.loadArticleList(homeDto,type);

        //4. 返回结果
        return ResponseResult.okResult(list);
    }

    /**
     * 同步自媒体文章到article数据库
     *
     * @param articleDto
     * @return
     */
    @Override
    public ResponseResult async(ArticleDto articleDto) {


        Long articleId = null;

        //1.根据articleDto的id字段有没有值，来决定是新增还是修改
        if (articleDto.getId() != null){
            articleId = articleDto.getId();
            //修改： ap_article   ap_article_content
            ApArticle apArticle = new ApArticle();
            BeanUtils.copyProperties(articleDto,apArticle);
            apArticleMapper.updateById(apArticle);

            ApArticleContent content = new ApArticleContent();
            content.setArticleId(apArticle.getId());
            content.setContent(articleDto.getContent());
            apArticleContentMapper.update(content, Wrappers.<ApArticleContent>lambdaUpdate()
                    .set(ApArticleContent::getContent,articleDto.getContent())
                    .eq(ApArticleContent::getArticleId,content.getArticleId()));

        }else{
            //新增： ap_article   ap_article_config  ap_article_content
            ApArticle apArticle = new ApArticle();
            BeanUtils.copyProperties(articleDto,apArticle);
            apArticleMapper.insert(apArticle);

            articleId = apArticle.getId();
            ApArticleConfig apArticleConfig = new ApArticleConfig(apArticle.getId());
            apArticleConfigMapper.insert(apArticleConfig);

            ApArticleContent content = new ApArticleContent();
            content.setArticleId(apArticle.getId());
            content.setContent(articleDto.getContent());
            apArticleContentMapper.insert(content);
        }

        //生成文章详情页
        freemarkerService.buildArticlePage(articleId,articleDto.getContent());

        return ResponseResult.okResult(articleId);
    }

    /**
     * 加载首页
     *
     * @param homeDto
     * @param type
     * @return
     */
    @Override
    public ResponseResult loadIndex(ArticleHomeDto homeDto, int type) {
        //1. 查redis，拿热点文章返回
        String hotArticleListJSON = cacheService.get(ArticleConstants.HOT_ARTICLE + homeDto.getTag());
        if (StringUtils.isNotBlank(hotArticleListJSON)) {
            List<ArticleVo> articleVoList = JSON.parseObject(hotArticleListJSON, List.class);
            return ResponseResult.okResult(articleVoList);
        }else {
            //2. 如果redis没有，再查数据库
            return load(homeDto,type);
        }
    }
}

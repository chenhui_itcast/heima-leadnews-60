package com.heima.article.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.article.mapper.ApArticleConfigMapper;
import com.heima.article.service.ApArticleConfigService;
import com.heima.model.article.pojos.ApArticleConfig;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @author 陈辉
 * @data 2023 11:28
 */
@Service
public class ApArticleConfigServiceImpl extends ServiceImpl<ApArticleConfigMapper, ApArticleConfig> implements ApArticleConfigService {
    /**
     * 文章上下架
     *
     * @param map
     */
    @Override
    public void isDown(Map map) {
        //1. 拿到articleId和enable
        String articleId = String.valueOf(map.get("articleId"));
        String enable = String.valueOf(map.get("enable"));

        //是否下架，默认：下架
        boolean isDown = true;

        if (enable.equals("1")){
            isDown = false;
        }

        //2. 根据articleId去修改对应的文章配置
        update(Wrappers.<ApArticleConfig>lambdaUpdate()
                .set(ApArticleConfig::getIsDown,isDown)
                .eq(ApArticleConfig::getArticleId,articleId));
    }
}

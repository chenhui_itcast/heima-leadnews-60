package com.heima.article.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.article.pojos.ApArticleConfig;

import java.util.Map;

/**
 * @author 陈辉
 * @data 2023 11:27
 */
public interface ApArticleConfigService extends IService<ApArticleConfig> {
    /**
     * 文章上下架
     * @param map
     */
    void isDown(Map map);
}

package com.heima.article.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.article.pojos.ApArticleContent;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author 陈辉
 * @data 2023 11:25
 */
@Mapper
public interface ApArticleContentMapper extends BaseMapper<ApArticleContent> {
}

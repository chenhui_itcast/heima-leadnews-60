package com.heima.article.listener;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.heima.article.service.ApArticleConfigService;
import com.heima.common.constants.WmNewsMessageConstants;
import com.heima.model.article.pojos.ApArticleConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author 陈辉
 * @data 2023 15:36
 */

@Component
public class ArticleListener {
    @Autowired
    private ApArticleConfigService apArticleConfigService;

    /**
     * 文章上下架
     * @param msg
     */
    @KafkaListener(topics = WmNewsMessageConstants.WM_NEWS_UP_OR_DOWN_TOPIC)
    public void downOrUpArticle(String msg){
        if (StringUtils.isNotBlank(msg)) {
            Map map = JSON.parseObject(msg, Map.class);
            apArticleConfigService.isDown(map);
        }
    }
}

package com.heima.article.service;

/**
 * @author jack
 * @data 2023 14:40
 */
public interface HotArticleService {
    /**
     * 计算热点文章
     */
    void computeHotArticle();
}

package com.heima.article.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author jack
 * @data 2023 16:03
 */

@RestController
@RequestMapping
public class PingController {

    @GetMapping("/ping")
    public String ping(){
        return "pong";
    }
}

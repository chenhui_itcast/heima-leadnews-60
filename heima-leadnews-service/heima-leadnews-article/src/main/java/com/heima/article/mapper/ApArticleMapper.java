package com.heima.article.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.article.dtos.ArticleHomeDto;
import com.heima.model.article.pojos.ApArticle;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author 陈辉
 * @data 2023 11:21
 */
@Mapper
public interface ApArticleMapper extends BaseMapper<ApArticle> {
    //加载文章首页   type  1
    //加载更多       type  1
    //加载更新       type  2
    List<ApArticle> loadArticleList(@Param("homeDto") ArticleHomeDto homeDto,
                                    @Param("type") int type);
}

package com.heima.article.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.article.dtos.ArticleDto;
import com.heima.model.article.dtos.ArticleHomeDto;
import com.heima.model.article.pojos.ApArticle;
import com.heima.model.common.dtos.ResponseResult;

/**
 * @author 陈辉
 * @data 2023 11:21
 */
public interface ApArticleService extends IService<ApArticle> {
    /**
     * 加载首页/加载更新/加载更多
     * @param homeDto
     * @param type
     * @return
     */
    ResponseResult load(ArticleHomeDto homeDto, int type);

    /**
     * 加载首页
     * @param homeDto
     * @param type
     * @return
     */
    ResponseResult loadIndex(ArticleHomeDto homeDto, int type);

    /**
     * 同步自媒体文章到article数据库
     *
     * @param articleDto
     * @return
     */
    ResponseResult async(ArticleDto articleDto);
}

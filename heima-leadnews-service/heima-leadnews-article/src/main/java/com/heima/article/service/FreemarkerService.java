package com.heima.article.service;

import com.heima.model.article.pojos.ApArticle;

/**
 * @author 陈辉
 * @data 2023 16:18
 */
public interface FreemarkerService {

    /**
     * 生成文章详情页
     * @param articleId
     * @param content
     */
    void buildArticlePage(Long articleId, String content);
}

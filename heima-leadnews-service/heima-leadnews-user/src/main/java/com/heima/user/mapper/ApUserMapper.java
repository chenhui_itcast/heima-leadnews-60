package com.heima.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.user.pojos.ApUser;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author 陈辉
 * @data 2023 17:25
 */
@Mapper
public interface ApUserMapper extends BaseMapper<ApUser> {
}

package com.heima.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.user.dtos.LoginDto;
import com.heima.model.user.pojos.ApUser;

/**
 * @author 陈辉
 * @data 2023 17:26
 */
public interface ApUserService extends IService<ApUser> {
    /**
     * app用户登录
     *
     * @param loginDto
     * @return
     */
    ResponseResult login(LoginDto loginDto);
}

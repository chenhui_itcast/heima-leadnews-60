package com.heima.user.controller;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.user.dtos.LoginDto;
import com.heima.user.service.ApUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 陈辉
 * @data 2023 16:18
 */
@RestController
@RequestMapping("/api/v1/login")
@Slf4j
@Api(tags = "app登录模块")
public class LoginController {

    @Autowired
    private ApUserService apUserService;

    @ApiOperation("app登录接口")
    @PostMapping("/login_auth")
    public ResponseResult loginAuth(@RequestBody LoginDto loginDto) {
        log.info("app登录：{}", loginDto);
        return apUserService.login(loginDto);
    }
}

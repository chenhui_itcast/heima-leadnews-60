package com.heima.user.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.user.dtos.LoginDto;
import com.heima.model.user.pojos.ApUser;
import com.heima.utils.common.AppJwtUtil;
import com.heima.user.mapper.ApUserMapper;
import com.heima.user.service.ApUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 陈辉
 * @data 2023 17:26
 */
@Service
@Transactional
@Slf4j
public class ApUserServiceImpl extends ServiceImpl<ApUserMapper, ApUser> implements ApUserService {
    /**
     * app用户登录
     *
     * @param loginDto
     * @return
     */
    @Override
    public ResponseResult login(LoginDto loginDto) {
        //0. 参数校验
        if (StringUtils.isEmpty(loginDto.getPhone()) && StringUtils.isEmpty(loginDto.getPassword())){
            //游客：不用校验，直接基于固定id - 0  生成token返回给前端
            String token = AppJwtUtil.getToken(0L);
            Map<String, Object> resultMap = new HashMap<>();
            resultMap.put("token",token);
            return ResponseResult.okResult(resultMap);

        }else if (!StringUtils.isEmpty(loginDto.getPhone()) && !StringUtils.isEmpty(loginDto.getPassword())){
            //正常登录
            //1. 根据手机号查询用户信息
            ApUser apUser = getOne(Wrappers.<ApUser>lambdaQuery().eq(ApUser::getPhone, loginDto.getPhone()));

            //2. 判断用户是否存在
            //2.1 不存在： 直接结束，返回： 用户信息不存在!
            if (apUser == null) {
                return ResponseResult.errorResult(AppHttpCodeEnum.USER_NOT_EXIST);
            }

            //3. 拿用户输入的密码和数据库中盐拼接，拼接后加密，得到密文
            String inputPwd = DigestUtils.md5DigestAsHex((loginDto.getPassword() + apUser.getSalt()).getBytes());

            //4. 将计算的密文和数据库中存储的密文进行比对
            //4.1 不一致： 直接结束，返回：密码错误!
            if (!inputPwd.equals(apUser.getPassword())) {
                return ResponseResult.errorResult(AppHttpCodeEnum.LOGIN_PASSWORD_ERROR);
            }

            //5. 登录成功，基于用户id生成token返回给前端
            String token = AppJwtUtil.getToken(Long.valueOf(apUser.getId()));

            //6. 登录成功，将用户信息也返回给前端
            Map<String,Object> userMap = new HashMap<>();
            userMap.put("id",apUser.getId());
            userMap.put("name",apUser.getName());
            userMap.put("phone",apUser.getPhone());

            //7. 封装一个大的map，将token和user信息都封装进去
            Map<String, Object> resultMap = new HashMap<>();
            resultMap.put("user",userMap);
            resultMap.put("token",token);
            return ResponseResult.okResult(resultMap);

        }else{
            //瞎搞
            return ResponseResult.okResult(AppHttpCodeEnum.PARAM_INVALID);
        }
    }
}

package com.heima.jvm.controller;

import com.heima.jvm.pojos.Student;
import com.heima.jvm.service.JVMService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

/**
 * @author jack
 * @data 2023 11:33
 */
@RestController
public class JVMController {


    @Autowired
    private JVMService jvmService;


    @GetMapping("/jvm")
    public String testJVM(){


        //GC Root对象
        ArrayList<Student> list = new ArrayList<>();

        for (int i = 0; i < 3_000_000; i++) {
            Student stu = new Student();
            stu.setName("jack"+i);
            stu.setAge(i);
            stu.setSex(i);
            list.add(stu);
            list.add(new Student());
        }

        new Student();

       // System.gc();        //垃圾回收

        return "success:"+ list.size();
    }



    @GetMapping("/jvm1")
    public String testJVM1(String msg) {

        if ("abc".equals(msg)) {
            //只要对象被栈内存中的变量引用，那么这个对象称之为：GC Root对象
            ArrayList<Student> list = new ArrayList<>();

           /* while (msg.equals("abc")) {
                int size = list.size();
            }*/
       /*Thread t1 = new Thread(() -> {
            synchronized ("abc") {
                try {
                    Thread.sleep(1000);

                    synchronized ("aaa") {
                        System.out.println("线程1获得了所有锁");
                    }
                    ;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });


        Thread t2 = new Thread(() -> {
            synchronized ("cba") {
                try {
                    Thread.sleep(1000);

                    synchronized ("ccc") {
                        System.out.println("线程2获得了所有锁");
                    }
                    ;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        t1.setName("t1");
        t2.setName("t2");

        t1.start();
        t2.start();*/

            //int a = 1 / 0 ;
            return "ok";
        } else if ("666".equals(msg)) {
            jvmService.testSleep();
            return "slow";
        } else {
            int a = 1 / 0;
            return "error";
        }
    }
}

package com.heima.jvm.sjms.factory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * @author jack
 * @data 2023 15:13
 */
@Service
public class UserServiceImpl implements UserService{

    //需求： 放oracle存
    //需求： 放mysql存
    //代码耦合度过高： service和mapper的耦合
    //怎么解：工厂模式+多态
//    @Autowired
//    private UserMapper1 userMapper;
//    private UserMapper2 userMapper;

    @Autowired
    @Qualifier("userMapper2")
    private UserMapper userMapper;


    @Override
    public void save() {
        userMapper.save();
    }
}

package com.heima.jvm.pojos;

import lombok.Data;

/**
 * @author jack
 * @data 2023 11:32
 */
@Data
public class Student {
    private String name;
    private Integer age;
    private Integer sex;
}

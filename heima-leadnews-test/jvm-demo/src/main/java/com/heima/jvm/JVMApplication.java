package com.heima.jvm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author jack
 * @data 2023 11:31
 */
@SpringBootApplication
public class JVMApplication {
    public static void main(String[] args) {
        SpringApplication.run(JVMApplication.class,args);
    }
}

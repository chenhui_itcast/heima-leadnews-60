package com.heima.jvm.collections;

/*
       1. FailFast机制：
			a. FailFast指的是一边基于迭代器遍历集合，一边修改集合的长度，
				这时候集合会抛出一个并发修改异常（ConcurrentModificationException），让操作快速失败！
			b. 本质是因为在集合源码底层有一个校验：
				比较 if(expectedModCount !=  ModCount	)，
				如果不一致，就会throw ConcurrentModificationException()
				expectedModCount: 集合预期修改次数
				ModCount: 预期实际修改次数
			c. 针对 ArrayList，LinkedList，HashSet,HashMap等常用集合不适用于并发编程。

			CAP： CP: 强一致性    AP: 高可用性

		2. unSafe机制：
			a. 在多线程并发访问时，一边修改集合长度，一遍遍历读取集合内容操作被允许，但是可能会导致读到的数据不安全。
			b. 实现原理：因为迭代器遍历访问的是snapshot数组，而list.add方法每次添加数据前会 Arrays.copyOf操作，产生一个
				newElements新的对象数据，从而实现数据的隔离，从而保证数据可用性。
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CopyOnWriteArraySet;

public class Demo1 {
    public static void main(String[] args) throws InterruptedException {

//        HashMap<String,String> hashMap = new HashMap<>();
        ConcurrentHashMap<String,String> hashMap = new ConcurrentHashMap<>();

//        HashSet<String> set = new HashSet<>();
//        CopyOnWriteArraySet<String> set = new CopyOnWriteArraySet<>();

//        ArrayList<String> list = new ArrayList<>();
        CopyOnWriteArrayList<String> list = new CopyOnWriteArrayList<>();
        list.add("a");
        list.add("b");
        list.add("c");
        list.add("d");


        new Thread(()->{
            System.out.println("t1开始执行...");
            for (String s : list) {
                if (s.equals("a")) {
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                System.out.println(s);
            }
            System.out.println("t1执行完毕...");
        }).start();

        Thread.sleep(10);

        new Thread(()->{
            list.add("hello");
            System.out.println("t2执行完毕...");
        }).start();


       /* Iterator<String> it = list.iterator();
        while (it.hasNext()){
            String s = it.next();
            if (s.equals("a")){
                list.remove(0);
            }


            System.out.println("s = " + s);
        }*/

    }
}

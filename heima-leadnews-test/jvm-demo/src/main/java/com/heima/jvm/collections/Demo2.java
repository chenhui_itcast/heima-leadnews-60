package com.heima.jvm.collections;

/*


0. ArrayList构造过程
    1. 给elementData赋值了默认长度为0的空数组

1. ArrayList的添加过程
            Object[] elementData： ArrayList底层用来存储数据的对象数组。
        1. new ArrayList<>()底层会创建一个长度为0的空数组，赋值给elementData。
        2. 当调用集合的add添加元素方法时，方法内会先判断集合底层数组有没有存满：
            存满了：需要扩容，调用grow()方法对elementData数组进行扩容
        3. 将要添加的元素存入数组对应size的索引位置
        4. 将集合中的元素个数size + 1

    2. ArrayList的扩容机制
        1. 第一次添加，会将数组扩容为一个长度为10的新数组。
        2. 后续扩容，按照1.5倍的系数扩容。

        注意： 数组的扩容最大只能扩到Integer.MAX_VALUE

        第一次：0-->10
        第二次：10-->15
        第三次：15-->22
 */

import java.util.ArrayList;

public class Demo2 {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>();

        list.add("hello");
    }
}

package com.heima.jvm.sjms.factory;

/**
 * @author jack
 * @data 2023 15:13
 */
public interface UserService {
    void save();
}

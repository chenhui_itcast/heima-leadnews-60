package com.heima.jvm.sjms.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.List;

/**
 * @author jack
 * @data 2023 15:42
 */

public class Demo {
    public static void main(String[] args) {
        List<String> list  = new ArrayList<>();
        List<String> proxyList = (List<String>) Proxy.newProxyInstance(list.getClass().getClassLoader(),
                list.getClass().getInterfaces(),
                //事件监听：每拿代理对象调一次方法，就会进入当前InvocationHandler内执行一次invoke方法逻辑
                new InvocationHandler() {
                    @Override
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                        System.out.println("当前正在添加:"+args[0]);
                        return method.invoke(list,args);
                    }
                });

        //需求：在不改变add功能基础上，增加额外功能：每添加一个元素，就记一次日志：当前正在添加：xxx
        proxyList.add("abc");
        proxyList.add("hello");
        proxyList.add("world");
        proxyList.add("java");

        System.out.println("list = " + list);
    }
}

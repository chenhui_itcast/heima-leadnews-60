package com.heima.jvm.sjms.factory;

/*
    工厂模式： 专门对外生产指定类型的对象!
        UserMapper工厂：专门生产UserMapper对象
 */

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.Set;

public class UserMapperFactory {

    public static UserMapper getUserMapper(){
        try {
            //1. 读取配置文件
            Properties properties = new Properties();
            properties.load(new FileInputStream("D:\\IdeaProjects\\heima-leadnews\\heima-leadnews-test\\jvm-demo\\src\\main\\java\\com\\heima\\jvm\\sjms\\factory\\user.properties"));

            //2. 解析配置，创建对应的bean
            Set<Object> keys = properties.keySet();
            for (Object key : keys) {
                String keyStr = String.valueOf(key);
                String beanClassName = properties.getProperty(keyStr);

                Class clazz = Class.forName(beanClassName);
                Object bean = clazz.newInstance();
                return (UserMapper) bean;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

}

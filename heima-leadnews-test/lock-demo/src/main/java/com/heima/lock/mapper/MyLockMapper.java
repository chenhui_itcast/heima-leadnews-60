package com.heima.lock.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.lock.pojos.MyLock;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author jack
 * @data 2023 10:33
 */
@Mapper
public interface MyLockMapper extends BaseMapper<MyLock> {
}

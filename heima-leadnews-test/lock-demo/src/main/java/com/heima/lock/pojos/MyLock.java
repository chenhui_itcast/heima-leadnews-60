package com.heima.lock.pojos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author jack
 * @data 2023 10:23
 */
@TableName("mylock")
@Data
public class MyLock {

    @TableId(type = IdType.AUTO)
    private Integer id;

    private String lockName;

    private String lockValue;

    public MyLock(String lockName, String lockValue) {
        this.lockName = lockName;
        this.lockValue = lockValue;
    }

    public MyLock() {
    }
}

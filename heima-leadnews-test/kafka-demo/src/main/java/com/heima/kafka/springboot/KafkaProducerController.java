package com.heima.kafka.springboot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 陈辉
 * @data 2023 11:36
 */
@RestController
public class KafkaProducerController {
    @Autowired
    private KafkaTemplate<String,String> kafkaTemplate;

    @GetMapping("/producer")
    public String sendMsg(){
        kafkaTemplate.send("boot-topic","key1","hello,springboot with kafka");
        return "send is ok!";
    }
}

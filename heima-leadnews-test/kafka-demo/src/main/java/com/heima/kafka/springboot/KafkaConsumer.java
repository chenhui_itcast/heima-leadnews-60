package com.heima.kafka.springboot;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

/**
 * @author 陈辉
 * @data 2023 11:39
 */
@Component
public class KafkaConsumer {

    /*@KafkaListener(topics="boot-topic")
    public void getMsg(String msg){
        System.out.println("msg = " + msg);
    }*/

    @KafkaListener(topics="boot-topic")
    public void getMsg(ConsumerRecord<Object,Object> record){
        Object key = record.key();
        System.out.println("key = " + key.toString());
        Object value = record.value();
        System.out.println("value = " + value.toString());
    }

}

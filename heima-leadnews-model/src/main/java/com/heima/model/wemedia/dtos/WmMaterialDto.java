package com.heima.model.wemedia.dtos;

import com.heima.model.common.dtos.PageRequestDto;
import lombok.Data;

/**
 * @author 陈辉
 * @data 2023 16:52
 */
@Data
public class WmMaterialDto extends PageRequestDto {
    private String isCollection;        //是否收藏
}

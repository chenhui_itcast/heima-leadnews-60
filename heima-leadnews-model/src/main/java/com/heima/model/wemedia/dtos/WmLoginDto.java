package com.heima.model.wemedia.dtos;

import lombok.Data;

/**
 * @author 陈辉
 * @data 2023 10:05
 */
@Data
public class WmLoginDto {
    private String name;
    private String password;
}

package com.heima.model.user.dtos;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author 陈辉
 * @data 2023 16:22
 */
@Data
@ApiModel("app登录的请求体参数")
public class LoginDto {
    @ApiModelProperty("手机号")
    private String phone;   //手机号
    @ApiModelProperty("密码")
    private String password;  //密码
}

package com.heima.model.article.vos;

import com.heima.model.article.pojos.ApArticle;
import lombok.Data;

/**
 * @author jack
 * @data 2023 15:07
 */
@Data
public class ArticleVo extends ApArticle {
    private int score;      //分数
}

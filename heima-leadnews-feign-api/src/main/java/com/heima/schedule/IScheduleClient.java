package com.heima.schedule;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.schedule.dtos.TaskDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author 陈辉
 * @data 2023 16:45
 */
@FeignClient(value = "leadnews-schedule")
public interface IScheduleClient{

    /**
     * 添加任务
     *
     * @param taskDto
     * @return
     */
    @PostMapping("/api/v1/schedule/addTask")
    ResponseResult addTask(@RequestBody TaskDto taskDto);


    /**
     * 拉取任务
     *
     * @param taskType
     * @param priority
     * @return
     */
    @GetMapping("/api/v1/schedule/pullTask")
    ResponseResult pullTask(@RequestParam Integer taskType,
                            @RequestParam Integer priority);

}

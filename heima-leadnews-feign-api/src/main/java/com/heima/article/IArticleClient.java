package com.heima.article;

import com.heima.article.fallback.ArticleClientFallback;
import com.heima.model.article.dtos.ArticleDto;
import com.heima.model.common.dtos.ResponseResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author 陈辉
 * @data 2023 14:44
 */
@FeignClient(value = "leadnews-article",fallback = ArticleClientFallback.class)
public interface IArticleClient {

    /**
     * article文章同步接口
     * @param articleDto
     * @return
     */
    @PostMapping("/api/v1/article/async")
    public ResponseResult saveOrUpdateArticle(@RequestBody ArticleDto articleDto);
}

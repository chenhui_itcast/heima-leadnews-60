package com.heima.article.fallback;

import com.heima.article.IArticleClient;
import com.heima.model.article.dtos.ArticleDto;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import org.springframework.stereotype.Component;

/**
 * @author 陈辉
 * @data 2023 14:48
 */
@Component
public class ArticleClientFallback implements IArticleClient {
    /**
     * article文章同步接口
     *
     * @param articleDto
     * @return
     */
    @Override
    public ResponseResult saveOrUpdateArticle(ArticleDto articleDto) {
        return ResponseResult.errorResult(AppHttpCodeEnum.SERVER_ERROR);  //降级逻辑
    }
}
